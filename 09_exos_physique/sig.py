import matplotlib.pyplot as plt     
import numpy as np
import random as rd
import math

from IPython.display import clear_output
from ipywidgets import *


                

def exo():
    T = rd.randint(1,6)                      
    D = math.pi/rd.randint(1,6)             
    A = rd.randint(1,12) 
    plt.figure(figsize=(15,6)) 
    X = np.arange(0,8,0.01)
    Y = [A * math.cos(2 * math.pi / T * x + D) for x in X]
    plt.plot(X,Y)
    axes = plt.gca()
    axes.xaxis.set_ticks(np.arange(0,8.5, 0.5))  

    plt.grid(True)
    plt.show()
    
    Tval=widgets.Text(
    value='',
    placeholder='',
    description='période :',
    disabled=False
    )

    Aval=widgets.Text(
    value='',
    placeholder='',
    description='Amplitude :',
    disabled=False
    )
    
    Tbutt = widgets.Button(description='vérifier')
    Toutt = widgets.Output()

    Abutt = widgets.Button(description='vérifier')
    Aoutt = widgets.Output()
    
    def on_clicked_T(b):
        with Toutt:
            clear_output()
            try:
                int(Tval.value)                            
                if int(Tval.value) == T:
                    res = 'bravo!'
                else:
                    res = 'mauvaise réponse...'
                print(res)
            except:
                print('il faut saisir un nombre entier')

    def on_clicked_A(b):
        with Aoutt:
            clear_output()
            try:
                int(Aval.value)                            
                if int(Aval.value) == A:
                    res = 'bravo!'
                else:
                    res = 'mauvaise réponse...'
                print(res)
            except:
                print('il faut saisir un nombre entier')
    
    Tval.on_submit(on_clicked_T)    
    Tbutt.on_click(on_clicked_T)

    Aval.on_submit(on_clicked_A)    
    Abutt.on_click(on_clicked_A)
    return widgets.VBox([Aval,Abutt,Aoutt,Tval,Tbutt,Toutt])


def trace(periode,amplitude,dephasage):
    plt.figure(figsize=(15,8)) 
    X = np.arange(0,8,0.01)
    Y = [amplitude * math.cos(2 * math.pi / periode * x + dephasage) for x in X]
    plt.plot(X,Y)
    axes = plt.gca()
    axes.xaxis.set_ticks(np.arange(0,8.5, 0.5))  
    axes.yaxis.set_ticks(np.arange(-13,14,1))

    plt.grid(True)
    plt.show()
    
def exo2():
    interact(trace, periode=(1,6,1), amplitude=(1,12,1),dephasage=(0,3,0.5))
    